# Expense Settlement Task, Steps to Solve it.

DISCLAIMER: First attempt to solve this problem so obviously improvement is needed. 
So use it at your own risk. Chillars are not allowed in this version.

1. Calculate the Sum of expenses and Divide it by no. of person. [ Expense Per Person ]
```TOTAL EXPENSES :  {'a': 400, 'b': 1000, 'c': 100, 'd': 900}```

2. Calculate mean, -ve shows person will get that amount and +ve shows person will pay that amount.
```NEGATIVE ACCOUNTS :  {'a': 200, 'b': -400, 'c': 500, 'd': -300}```

3. Sort debts
 ```SORTED DEBTS  [('b', -400), ('d', -300), ('a', 200), ('c', 500)]```

4. Start with -ve amount, and assign +ve amount to it.
```a is paying 200 to b with balance -400 ```
5. Repeat the above step untill all the account turns to 0 and store who is paying what in different stack.
```
SETTLEMENT : 
{
    'a': [{'b': -200}], 
    'b': [], 
    'c': [{'d': -300}, {'b': -200}], 
    'd': []
}
A will pay 200/- to B
B doesn't have to pay anything 
C will pay 300/- to D and 200/- to B
D doesn't have to pay anything.
```
# TODO
##### Got the relation who is paying to whom, by using Direct Acyclic Graph ( DAG ) smart-settlement can be done.

```
example, 
A --> 10 --> C 
C --> 10 --> B
Then B will give 10/- to A
```