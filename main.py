"""
1. Calculate the Sum of expenses and Divide it by no. of person. [ Expense Per Person ]
TOTAL EXPENSES :  {'a': 400, 'b': 1000, 'c': 100, 'd': 900}

2. Calculate mean, -ve shows person will get that amount and +ve shows person will pay that amount.
NEGATIVE ACCOUNTS :  {'a': 200, 'b': -400, 'c': 500, 'd': -300}

3. Sort debts
SORTED DEBTS  [('b', -400), ('d', -300), ('a', 200), ('c', 500)]

4. Start with -ve amount, and assign +ve amount to it.
---
a is paying 200 to b with balance -400
a left with 0/- amount

5. Repeat the above step untill all the account turns to 0 and store who is paying what in different stack.
SETTLEMENT : {'a': [{'b': -200}], 'b': [], 'c': [{'d': -300}, {'b': -200}], 'd': []}

A will pay 200/- to B
B doesn't have to pay anything 
C will pay 300/- to D and 200/- to B
D doesn't have to pay anything.

# TODO : Got the relation who is paying to whom, by using Direct Acyclic Graph ( DAG ) smart-settlement can be done.
# example, 
# A --> 10 --> C 
# C --> 10 --> B
# Then B will give 10/- to A
"""
who_is_paying_what_to_whom = {}

def easy_debt_settlement(debt_from_expenses, expenses):
    # sorting least -ve accounts and +ve account
    sorted_debt = sorted(debt_from_expenses.items(), key=lambda value: value[1])
    print ('SORTED DEBTS ', sorted_debt)

    person_to_get = ''
    person_to_get_amount = 0
    for person, amount in sorted_debt:        
        if debt_from_expenses[person] == 0:
            continue
        if amount < 0 and amount < person_to_get_amount:
            person_to_get = person
            person_to_get_amount = amount
            continue
        if amount > person_to_get_amount and amount > 0:
            print ('{} is paying {} to {} with balance {}'.format(person, amount, person_to_get, person_to_get_amount))
            settlement_amount = amount + person_to_get_amount
            if settlement_amount < 0:
                debt_from_expenses[person] = debt_from_expenses[person] + settlement_amount
                who_is_paying_what_to_whom[person].append({person_to_get:settlement_amount})
                debt_from_expenses[person_to_get] = debt_from_expenses[person_to_get] + amount
            else:
                debt_from_expenses[person] = debt_from_expenses[person] + person_to_get_amount
                who_is_paying_what_to_whom[person].append({person_to_get:person_to_get_amount})
                debt_from_expenses[person_to_get] = debt_from_expenses[person_to_get] - person_to_get_amount
            
            print ("{} left with {}/- amount".format(person, debt_from_expenses[person]))

            # check, if there is still -ve balances.
            for item in debt_from_expenses.values():
                if item < 0:
                    # try to settle debt again for -ve 
                    easy_debt_settlement(debt_from_expenses, expenses)

def calculate_diff():
    expenses = {"a":400,"b":1000,"c":100,"d":900}

    sum_of_members_expenses = sum(expenses.values())
    per_head_expense = int(sum_of_members_expenses/len(expenses.keys()))
    
    debt_from_expenses = expenses.copy()
    
    for key, value in debt_from_expenses.items():
        who_is_paying_what_to_whom[key]=[]
        debt_from_expenses[key] = per_head_expense - value
    print ('TOTAL EXPENSES : ', expenses)
    print ('NEGATIVE ACCOUNTS : ', debt_from_expenses)
    return debt_from_expenses, expenses

debt_from_expenses, expenses = calculate_diff()
easy_debt_settlement(debt_from_expenses, expenses)
print ('SETTLEMENT :', who_is_paying_what_to_whom)